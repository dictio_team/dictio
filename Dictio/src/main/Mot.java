package main;
/**
*
* Author: Pierre-Olivier Morin	
*/

public class Mot {
	private String mot;
	private String description;
	
	public Mot(String mot, String description) {
		this.mot= mot;
		this.description=description;
	}
	public String get_mot() {
		return mot;
	}
	public void set_mot(String _mot) {
		this.mot = _mot;
	}
	public String get_description() {
		return description;
	}
	public void set_description(String _description) {
		this.description = _description;
	}
}
