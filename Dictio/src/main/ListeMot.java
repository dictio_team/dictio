package main;
import java.io.*;
import java.util.List;
import java.util.LinkedList;

/**
 *
 * Author: Pierre-Olivier Morin	
 */
public class ListeMot {
	private List<String> _ListeMot = new LinkedList<String>();
	private List<ArbreLexico> _ListeArbreLexico = new LinkedList<ArbreLexico>();
	
	public ListeMot(String Filepath) {
		File file = new File(Filepath);
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader br = new BufferedReader(fileReader);
			String line;
			while((line = br.readLine())!= null) {
				this._ListeMot.add(line);				
			}
			br.close();
			fileReader.close();
			
			// // utile pour faire un foreach apr�s (foreach n�cessaire)
			for(String mot : this._ListeMot) {
				String[] motEtDescription = mot.split(" & ");
				mot = motEtDescription[0].toLowerCase();
				ArbreLexico arbreLexico = null;
				int compteurMots = 0;
				for(ArbreLexico unArbreLexico : this._ListeArbreLexico) {
					ArbreLexico.LexiNode head = unArbreLexico.getHead();
					System.out.println(this._ListeArbreLexico.size());
					if(mot.charAt(0) == head.getLettreCourante()) {
						arbreLexico = unArbreLexico;
						break;
					}
					if(compteurMots == this._ListeArbreLexico.size()) {
						arbreLexico = new ArbreLexico();
					}
					compteurMots++;
				}
				if(this._ListeArbreLexico.size() == 0) {
					arbreLexico = new ArbreLexico();
				}
				for(int i = 0; i<mot.length(); i++) {					
					if(i != mot.length()-1) {
						arbreLexico.add(mot.charAt(i), mot.charAt(i+1));
					}else {
						arbreLexico.add(mot.charAt(i));
					}
											
				}
				if(arbreLexico != null) {
					this._ListeArbreLexico.add(arbreLexico);
				}
				
		    }
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	public List<String> get_ListeMot() {
		return this._ListeMot;
	}
	
	public List<ArbreLexico> get_ListeArbreLexico(){		
		return this._ListeArbreLexico;
	}

}
