/**
 * 
 */
package main;
import javax.swing.*;

import java.awt.*;
import java.util.*; 
import java.util.List;
import java.awt.event.*;

/**
 * @author AQ57580
 *
 */
public class Affichage {
	
	private static boolean focusMotCourant = true;
	
	//private static JFrame frame; 
	public static void afficher() {
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame=new JFrame("Dictio"); 	
		JButton btnCharger = new JButton("Charger");     
		JButton btnEnregister = new JButton("Enregistrer");
		JButton btnAjouterModifier = new JButton("Ajouter/Modifier");
		
		frame.add(btnCharger); 
		frame.add(btnEnregister); 
		frame.add(btnAjouterModifier); 
		
		JPanel panelGeneral = new JPanel();
		panelGeneral.setBorder(BorderFactory.createLineBorder(Color.black));	
		JPanel panelNord = new JPanel();
		panelNord.setBorder(BorderFactory.createLineBorder(Color.black));	
		JPanel panelOuest = new JPanel();
		panelOuest.setBorder(BorderFactory.createLineBorder(Color.black));	
		JPanel panelEst = new JPanel();
		panelEst.setBorder(BorderFactory.createLineBorder(Color.black));	
		JPanel panelCentre = new JPanel();
		panelCentre.setBorder(BorderFactory.createLineBorder(Color.black));		
		
		panelOuest.setLayout(new BoxLayout(panelOuest, BoxLayout.Y_AXIS));
		JTextArea motsPossibles = new JTextArea();
		motsPossibles.setPreferredSize( new Dimension( 250, 350 ) );
		
		ListeMot oListeMots = new ListeMot("C:\\Users\\AQ57580\\Documents\\dictio\\Dictio\\src\\main\\Mots.txt");
		List<ArbreLexico> uneListeMot = oListeMots.get_ListeArbreLexico();
		Iterator<ArbreLexico> iter = uneListeMot.iterator();
		while(iter.hasNext()) {
			ArbreLexico ligne = iter.next();
			System.out.println(ligne.getHead().getLettreCourante());
		}		
		
		JTextField motCourant = new JTextField(20);		
		motCourant.setPreferredSize( new Dimension( 250, 20 ) );
		frame.addWindowListener( new WindowAdapter() {
		    public void windowOpened( WindowEvent e ){
		    	motCourant.requestFocus();
		    }
		});
		motCourant.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
            	focusMotCourant = true;
            }

            @Override
            public void focusLost(FocusEvent e) {
            	focusMotCourant = false;
            }
        });
		motCourant.addKeyListener(new KeyAdapter() {
	         public void keyReleased(KeyEvent e) {                
	            if(focusMotCourant == true) {
	            	//System.out.println(e.getKeyChar()); //retourne la lettre relacher
	            	//System.out.println(motCourant.getText()); // retourne le text au complet de la o� on �crit
	            	
	            }
	         }        
	    });

		panelOuest.add(motCourant);
		panelOuest.add(motsPossibles);
		
		panelEst.setLayout(new CardLayout());
		JTextArea listesMots = new JTextArea("Un mot\nDeuxieme mot");
		listesMots.setPreferredSize( new Dimension( 200, 20 ) );
		panelEst.add(listesMots);
		
		panelCentre.setLayout(new CardLayout());
		JTextArea descriptionMotCourant = new JTextArea("Description treeeeeeeeeeeeeeeeeeeeeeeeees longue");
		panelCentre.add(descriptionMotCourant);
		
		panelNord.setLayout(new FlowLayout());
		panelNord.add(btnCharger);
		panelNord.add(btnEnregister);
		
		panelGeneral.setLayout(new BorderLayout());
		panelGeneral.add(panelNord, BorderLayout.NORTH);
		panelGeneral.add(panelOuest, BorderLayout.WEST);
		panelGeneral.add(panelEst, BorderLayout.EAST);
		panelGeneral.add(panelCentre, BorderLayout.CENTER);
		panelGeneral.add(btnAjouterModifier, BorderLayout.SOUTH);
		
		frame.add(panelGeneral); 
		frame.setSize(1000, 500);  
		frame.setResizable(false);
		frame.setVisible(false); // SI TU VEUX PAS QUE LA PAGE APPARAISSE, MET setVisible(false)
		
			
	}

}
