/**
 * 
 */
package main;
import java.util.*;


/**
 * @author AQ57580
 *
 */
public class ArbreLexico {
	
	private LexiNode head;
	
	public class LexiNode {

		private String motCourant;
		private char lettreCourante;
		private LinkedList<LexiNode> listeEnfant;
		
		

		public LexiNode(char lettreCourante) {
			this.lettreCourante = lettreCourante;
			this.motCourant = null;
			this.listeEnfant = new LinkedList<LexiNode>();
		}
		
		public void addEnfant(LexiNode unEnfant) {
			
			if(!verifieEnfants(this.listeEnfant, unEnfant)) {
				this.listeEnfant.add(unEnfant);
			}
			//Collections.sort(listeEnfant); fonctionne pas, idk why
		}
		
		public LinkedList<LexiNode> getEnfant(){
			return this.listeEnfant;
		}
		
		public char getLettreCourante() {
			return this.lettreCourante;
		}
		
	}
	
	public ArbreLexico() {
		this.head = null;
	}
	
	//return true si la liste d'enfants poss�de d�ja l'enfant
	public boolean verifieEnfants(LinkedList<LexiNode> lesEnfants, LexiNode uneNode) {
		ListIterator<LexiNode> list_Iter = lesEnfants.listIterator(); 
		boolean uneLexiNodeSemblable = false;
		while(list_Iter.hasNext()){ 
           LexiNode lexiNodeCourante = list_Iter.next(); 
           if(lexiNodeCourante.getLettreCourante() == uneNode.getLettreCourante()) {
        	   uneLexiNodeSemblable = true;
        	   break; //quitte le while pour pas le faire rouler pour rien
           }
        }
		return uneLexiNodeSemblable;
	}
	
	public void add(char lettreCourante, char lettreEnfant) {
		LexiNode lexiNodeParent = new LexiNode(lettreCourante);
		LexiNode lexiNodeEnfant = new LexiNode(lettreEnfant);
		if(!verifieEnfants(lexiNodeParent.getEnfant(), lexiNodeEnfant)) {
			lexiNodeParent.addEnfant(lexiNodeEnfant);
		}
		if(this.head == null) {
			this.head = lexiNodeParent;
		}
	}
	
	public void add(char lettreCourante) {
		LexiNode lexiNodeParent = new LexiNode(lettreCourante);
		if(this.head == null) {
			this.head = lexiNodeParent;
		}else {
			
		}
	}
	
	public LexiNode getHead() {
		return this.head;
	}
	
}
